import ballerina/io;
import ballerina/http;


type result record {
    string status;
    string learnerId;
};

public type Subjects record {|
    string course;
    string score;
|};

public type Learner record {|
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    Subjects[] past_subjects;
|};

final json LearnerJson = 
{
    "username": "Kondjenim_", 
    "lastname": "Thomas", 
    "firstname": "Marthin", 
    "preferred_formats": ["audio", "video", "text"], 
        "past_subjects": [
            {"course": "Algorithms", 
                "score": "B+"
            }, 
            {
                "course": "Programming I", 
                "score": "A+"
            }
        ]
};



public function main() returns @tainted error? {
    http:Client httpClient = check new ("http://localhost:9090/learner");
    string learnerId = "";

    io:println("\n-----------POST request:---------------");
    json resp = check httpClient->post("/create", LearnerJson);
    result success = check resp.cloneWithType(result);
    learnerId = success.learnerId;
    io:println(resp.toJsonString());

    io:println("\n----------GET request:------------------");
    json firstResp = check httpClient->get("/findLearnerById?learnerId=" + learnerId);
    io:println(firstResp.toJsonString());
    json Materials = check httpClient->get("/learningMaterials");
    
    io:println("\nLearning Materials");
    io:println(Materials.toJsonString());

    io:println("\n------------PUT request:-------------------");
    Learner UpdateJson = check LearnerJson.cloneWithType(Learner);
    UpdateJson.firstname = "Kondjeni";
    UpdateJson.lastname = "Kennedy";
    json UpdateResp = check httpClient->put("/update?learnerId=" + learnerId, UpdateJson.toJson());
    io:println(UpdateResp.toJsonString());

    io:println("\n-------------GET request:--------------------");
    json SecondResp = check httpClient->get("/findLearnerById?learnerId=" + learnerId);
    io:println(SecondResp.toJsonString());
}
