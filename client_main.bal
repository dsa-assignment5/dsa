import ballerina/io;
import ballerina/grpc;

FunctionRepositoryClient repositoryClientEp = check new ("http://localhost:9091");


function readResponse(Show_all_with_criteriaStreamingClient streamingClient) returns error? {
    string? result = check streamingClient->receiveString();
    while !(result is ()) {
        io:println(result);
        result = check streamingClient->receiveString();
    }
}

public function main() returns error? {
    io:print("\n______________________add function information______________________");
    metadata metadataReq = {
        Version: "1.0.0", 
        fullNames: "Lorenzo", 
        email: "Lorenzo@gmail.com", 
        address: "Windhoek", 
        language: "Ballerina", 
        keywords: "gRPC,Websocket, Http2"};

    var addResponse = repositoryClientEp->add_new_fn(metadataReq);
    io:print(addResponse.ensureType(string));

    io:print("\n______________________add functions information______________________");
    metadata[] metadataReqs = [{
        Version: "2.0.0", 
        fullNames: "Reginald", 
        email: "Reginald@gmail.com", 
        address: "Windhoek", 
        language: "Ballerina", 
        keywords: "gRPC,Websocket, Http2"}, 
        {
        Version: "1.0.0", 
        fullNames: "Given", 
        email: "Eugenia@gmail.com", 
        address: "Windhoek", 
        language: "Ballerina", 
        keywords: "Service-SSL/TLS, Service-OAuth2, Service-JWT Auth"}];

    var streamingClient = check repositoryClientEp->add_fns();

    foreach var item in metadataReqs {
        check streamingClient->sendMetadata(item);
    }

    check streamingClient->complete();

    json? response = check streamingClient->receiveString();
    io:println(response);

    io:print("\n______________________delete function information______________________");
    var cancelResponse = repositoryClientEp->delete_fn("2.0.0");
    io:print(cancelResponse.ensureType(string));

    io:print("\n______________________show function information______________________");
    var specificfunction = repositoryClientEp->show_fn("1.0.0");
    io:print(specificfunction.ensureType(json));

    io:print("\n______________________show all functions information______________________");
    stream<string, grpc:Error?> result = check repositoryClientEp->show_all_fns("");

    check result.forEach(function(json str) {
        io:println(str);
    });

    io:print("\n______________________show all functions by creteria information______________________");
    Show_all_with_criteriaStreamingClient streamingClient01 = check repositoryClientEp->show_all_with_criteria();

    future<error?> f1 = start readResponse(streamingClient01);

    check streamingClient01->complete();

}
